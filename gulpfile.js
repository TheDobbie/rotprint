var gulp = require('gulp');

var gulpCopy = require('gulp-copy');
var gulpClean = require('gulp-clean');
var gulpMerge = require('gulp-merge');
var gulpConcat = require('gulp-concat');
var gulpReplace = require('gulp-replace');
var gulpRename = require('gulp-rename');
var watch = require('gulp-watch');
var gulpConnect = require('gulp-connect');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var rev = require('gulp-rev');

var SourcesJS = [];
var SourcesCSS = [];
var SourcesImages = [];
var SourcesFonts = [];
var VarReplace = [];
SourcesJS.push('./bower_components/jquery/dist/jquery.js');

SourcesJS.push('./bower_components/jquery-ui/jquery-ui.js');
SourcesCSS.push('./bower_components/jquery-ui/themes/blitzer/jquery-ui.css');
SourcesImages.push('./bower_components/jquery-ui/themes/blitzer/images/*');

SourcesJS.push('./bower_components/bootstrap/dist/js/bootstrap.js');
SourcesCSS.push('./bower_components/bootstrap/dist/css/bootstrap.css');

SourcesJS.push('./bower_components/fabric.js/dist/fabric.js');

// Colorpicker
SourcesJS.push('./bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js');
SourcesCSS.push('./bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css');
SourcesImages.push('./bower_components/bootstrap-colorpicker/dist/img/**');
VarReplace.push(['../img/bootstrap-colorpicker','../images/bootstrap-colorpicker']);

// Flaute Colorselector
SourcesJS.push('./components/flaute-bootstrap-colorselector/dist/bootstrap-colorselector.min.js');
SourcesCSS.push('./components/flaute-bootstrap-colorselector/dist/bootstrap-colorselector.min.css');

// Slider
SourcesJS.push('./bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.js');
SourcesCSS.push('./bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.css');

// Bootstrap Dialog
SourcesJS.push('./bower_components/bootstrap3-dialog/dist/js/bootstrap-dialog.js');
SourcesCSS.push('./bower_components/bootstrap3-dialog/dist/css/bootstrap-dialog.css');

// Justified-Gallery (http://miromannino.github.io/Justified-Gallery/)
SourcesJS.push('./bower_components/justifiedGallery/dist/js/jquery.justifiedGallery.js');
SourcesCSS.push('./bower_components/justifiedGallery/dist/css/justifiedGallery.css');

// Font-Awesome
SourcesCSS.push('./bower_components/components-font-awesome/css/font-awesome.css');
SourcesFonts.push('./bower_components/components-font-awesome/fonts/**');
VarReplace.push(['../fonts/fontawesome','fonts/fontawesome']);

var Timestamp = Math.floor(Date.now() / 1000);

gulp.task(
    'dist-clean', function() {
        return gulp.src('./dist/**', {read: false}).pipe(gulpClean());
    });

gulp.task('dist-concat-js', function() {
    SourcesJS.push("src/js/**.js");
    SourcesJS.push("src/js/**/**.js");
    return gulp .src(SourcesJS)
        .pipe(gulpConcat('app'+Timestamp+'.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task('dist-concat-css', function() {
    SourcesCSS.push("src/css/**.css");
    var stream = gulp.src(SourcesCSS).pipe(gulpConcat('style'+Timestamp+'.css'));
    VarReplace.forEach(function(repval,i) {
        stream = stream.pipe(gulpReplace(repval[0],repval[1]));
    });
    stream = stream.pipe(cleanCSS({compatibility: 'ie8'}))
    return stream.pipe(gulp.dest('./dist'));
});


gulp.task('dist-html', function() {
    // Copy php files and replace vars
    return gulp .src(['src/**/*.html','src/*.html'])
        .pipe(gulpReplace('{{TIMESTAMP}}',Timestamp))
        .pipe(gulpRename(
            function(path) {
                path.basename = path.basename.replace(/\.src/g, '');
                return path;
            })
        )
        .pipe(gulp.dest('dist/'))
        .pipe(gulp.src('./dist/**.src.html', {read: false}).pipe(gulpClean()));
});

gulp.task('dist-images', function() {
    // Copy php files and replace vars
    SourcesImages.push('src/images/**');
    return gulp .src(SourcesImages)
        .pipe(gulp.dest('dist/images'));
});

gulp.task('dist-fonts', function() {
    // Copy php files and replace vars
    SourcesFonts.push('src/fonts/**');
    return gulp .src(SourcesFonts)
        .pipe(gulp.dest('dist/fonts'));
});

gulp.task('dist-data', function() {
    // Copy php files and replace vars
    SourcesImages.push('src/data/**');
    return gulp .src(SourcesImages)
        .pipe(gulp.dest('dist/data'));
});

//gulp.watch('src/**.php', ['dist-php']);

//gulp.task('default', gulp.series(['dist-clean', 'dist-concat-js', 'dist-concat-css', 'dist-php']);
gulp.task('default', ['dist-clean']); //, 'dist-concat-js', 'dist-concat-css', 'dist-php']);

gulp.task('build', ['dist-concat-js', 'dist-concat-css', 'dist-html', 'dist-images', 'dist-fonts', 'dist-data']);

gulp.task('webserver', function() {
    gulpConnect.server({
        root: 'dist',
        livereload: true
    });
});