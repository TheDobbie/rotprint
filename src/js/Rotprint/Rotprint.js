/**
 * Created by ts on 31.10.2018.
 */
$( document ).ready(function() {

    var editorSettingsDefault = {
        maxHeight: 1100,
        maxWidth: 1000,
        sliderStep: 5
    };
    var height = $("#page-wrapper").height();
    var width =  $("#page-wrapper").width();

    function getFormElements(){
        var formElements = [
            ":input",
            ".btn",
            ":text",
            "span",
            "select"
        ];
        return formElements;
    }
    var rpFontFamilies = [
        "Arial",
        "Helvetica",
        "Verdana",
        "Times New Roman"
    ];

    setFontFamiliesHTML();
    var canvas = new fabric.Canvas("c", {
        height: height,
        width: width,
        preserveObjectStacking: true,
        selection: false
    });
    $(".colorpicker-component").colorpicker().on('changeColor',
        function(ev) {
        console.log();
        changeColor($(this).colorpicker('getValue', '#ffffff'))
    });
    $(".rpAddText").on("click",function(){
        addText(canvas);
    });
    $(".rpAddPicture").on("click",function(){
        console.log($("#img-dialog").html());
       $("#img-dialog").modal();
    });
    $(".canvas-wrapper").on("keydown", function (e) {
        console.log(e.keyCode)
        if (e.keyCode == 46) {
            delElement($(this));
        }
        if (e.keyCode >= 37 && e.keyCode <= 40) {
            var obj = canvas.getActiveObject();
            if (e.keyCode == 37) {
                obj.left -= 5;
            } else if (e.keyCode == 38) {
                obj.top -= 5;
            } else if (e.keyCode == 39) {
                obj.left += 5;
            } else if (e.keyCode == 40) {
                obj.top += 5;
            }
            canvas.renderAll();
        }
        e.preventDefault();
    });
    $("textarea[name=text]").on("keyup", function () {
        canvas.getActiveObject().text = "" + $(this).val() + "";
        canvas.renderAll();
    });
    $("span[data-text-attribute]:not(.disabled)").on("click", function () {
        if(!$(this).hasClass("active")) {
            var attrValue = $(this).data("text-attribute-value");
        }else{
            attrValue = "";
        }
        var parent = $(this).closest(".row");
        console.log(parent.attr("class").split(' '));
        if(parent.hasClass("rpTextAlign")){
            parent.find(".active").toggleClass("active");
        }

        canvas.getActiveObject().set($(this).data("text-attribute"), attrValue);
        canvas.renderAll();

        $(this).toggleClass("active");
    });
    $("#rpFontFamily").on("change", function(){
        var id = $(this).val();
        if(jQuery.isNumeric(id)) {
            setFontFamily(id)
        }
    });
    $("[data-addelement]:not(.disabled)").on("click", function () {
        switch ($(this).data("addelement")) {
            case "text":
                addText(canvas);
                break;
            case "image":
                getImgDialog($(this)).modal("show");
                Anzeige.setMotiveHtml();
                break;
        }
    });
    $(".del-element").on("click", function () {
        delElement();
    });
    $("#rpFontSize").on("keyup", function () {
        console.log($(this).val());
        setFontSize($(this).val());
    });

    $(".unsel-element").on("click",function () {
        unselElement();
    });

    $(".optButton").on("click", function () {
        var objActive = canvas.getActiveObject();
        if(objActive) {
            switch ($(this).data("layeropt")) {
                case "sendToBack":
                    canvas.sendToBack(objActive);
                    break;
                case "bringForward":
                    canvas.bringForward(objActive);
                    break;
                case "sendBackwards":
                    canvas.sendBackwards(objActive);
                    break;
                case "bringToFront":
                    canvas.bringToFront(objActive);
                    break;
            }
            canvas.renderAll();
        }
    });
    updateEditor();
    canvas.on({
        "object:selected": updateEditor,
        "object:removed": updateEditor,
        "selection:updated": updateEditor,
        "selection:cleared": updateEditor,
        "object:scaled": updateTextSize
    });

    var rect = new fabric.Rect({
        left: 100,
        top: 100,
        fill: 'red',
        width: 20,
        height: 20
    });

    /*
        FUNKTIONEN
     */
    function unselElement(){
        canvas.discardActiveObject();
        canvas.renderAll();
    }
    function delElement(){
        canvas.getActiveObject().remove();
        canvas.renderAll();
    }
    function setFontFamiliesHTML(){
        var fontOptions = "";
        $.each(rpFontFamilies,function(id,font){
            fontOptions += "<option value='"+id+"' style='font-family:"+font+"'>"+font+"</option>";
        });
        $("#rpFontFamily").html(fontOptions);
    }
    function setFontFamily(s){
        canvas.getActiveObject().set("fontFamily", rpFontFamilies[s]);
        canvas.renderAll();
    }
    function changeColor(color){
        canvas.getActiveObject().setColor(color);
        canvas.renderAll();
    }
    function addText(canvas) {
        var text = new fabric.Text('Neuer Text', {
            left: 10,
            top: 10,
            fill: "#00000",
            hasControls: true,
            lockUniScaling: true
        });
        canvas.add(text);
        canvas.setActiveObject(text);
    }

    var subMenus = ["images","objects","text","main"];
    function toggleSubMenu(showMenu){
        $.each(subMenus,function( index, name ){
            if(showMenu == name){
                $(".sidebar-"+name).show()
            }else{
                $(".sidebar-"+name).hide();
            }
        });
    }

    function updateEditor(){
        if(canvas.getActiveObject()) {
            switch(canvas.getActiveObject().get('type')) {
                case "text":
                    toggleSubMenu("text");
                    updateTextFields();
                    break;
                case "rect":
                    toggleSubMenu("objects");
                    break;

            }
        }else{
            toggleSubMenu("main");
        }
    }

    function updateColorField(activeObj){
        if(activeObj) {
            console.log(activeObj.fill);
            $(".colorpicker-component").colorpicker('setValue', activeObj.fill);
        }
    }

    function deactiveTextButtonsHTML(){
        $(".text-attribute").removeClass("active");
    }

    function updateTextFormat(activeObj){
        deactiveTextButtonsHTML();
        if(activeObj.fontStyle === "italic") $(".fontStyle").addClass("active");
        if(activeObj.textDecoration === "underline") $(".textDecoration").addClass("active");
        if(activeObj.fontWeight === "bold") $(".fontWeight").addClass("active");
    }

    function updateTextHTML(activeObj){
        $("#text").val(activeObj.get('text'));
    }

    function updateTextFields (){
        var activeObj = canvas.getActiveObject();

        deactiveTextButtonsHTML();
        updateColorField(activeObj);
        updateTextFormat(activeObj);
        updateTextHTML(activeObj);
        updateTextSizeHTML(activeObj);
    }
    function updateTextSizeHTML(activeObj){
        $("#rpFontSize").val(activeObj.fontSize);
    }
    function updateTextSize(){
        var activeObj = canvas.getActiveObject();
        updateTextSizeHTML(activeObj);
    }

    function setFontSize(s){
        var activeObj = canvas.getActiveObject();
        activeObj.fontSize = s;
        canvas.renderAll();
    }



// "add" rectangle onto canvas
    canvas.add(rect);
});